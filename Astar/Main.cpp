#include "SFML/Graphics.hpp"
#include "iostream"
#include <vector>
#include <time.h>
#include "Map.h"


int main()
{
	float dt;
	sf::Time elapsed;
	sf::Clock clock;
	clock.restart();
	srand(time(NULL));
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "Roomba Simulator 2017");

	Map map;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		dt = clock.restart().asSeconds();
		elapsed = sf::seconds(0.2f);
		srand(dt);
		window.clear();
		
		
		map.draw(window);
	//	sf::sleep(elapsed);
			map.update();

		window.display();
	}

	return 0;
}