#include "Map.h"

Map::Map()
{
	walls = 0;
	dirtAmount = 0;
	std::cout << "How wide do you want the map?\n";
	std::cin >> mapSize.x;
	mapSize.x += 2;
	std::cout << "\nHow tall do you want the map?\n";
	std::cin >> mapSize.y;
	mapSize.y += 2;
	std::cout << "\nHow far should the roomba be able to see in each direction?\n";
	std::cin >> range;

	do
	{
		std::cout << "\nHow often should walls appear (out of 100)?\n";
		std::cin >> walls;
	} 
	while (walls < 0 || walls >= 100);

	do
	{
		std::cout << "\nHow often should dirt appear (out of 100, total of walls and dirt has to be 100 or less)?\n";
		std::cin >> dirtAmount;
	} 
	while (dirtAmount < 0 || dirtAmount > 100 - walls);

	std::cout << "\nHow often should dirt re-appear (lower is more frequent)?\n";
	std::cin >> frequency;

	explored = false;
	tiles.resize(mapSize.x, std::vector<Tile>(mapSize.y));
	Tile temp;
	for (size_t i = 0; i < mapSize.x; i++)
	{
		for (size_t j = 0; j < mapSize.y; j++)
		{
			tiles[0].push_back(static_cast<Tile>(temp));
		}
	}
	targetPos = sf::Vector2i(0, 0);
	generate();
}

void Map::smallestOpen()
{
	int t = 100000;
	bool closed = false;
	if (!openList.empty())
	{
		for (int i = 0; i < openList.size(); i++)
		{
			if (openList.at(i) == targetPos)
			{
				roombaPos = openList.at(i);
				return;
			}
			closed = false;
			if (!closedList.empty())
			{
				for (int j = 0; j < closedList.size(); j++)
				{
					if (closedList.at(j) == openList.at(i))
					{
						closed = true;
					}
				}
			}
			if (tiles[openList.at(i).x][openList.at(i).y].F <= t && !closed)
			{
				t = tiles[openList.at(i).x][openList.at(i).y].F;
				roombaPos = openList.at(i);
			}
		}
		closedList.push_back(roombaPos);
	}
	else
	{
		exit(0);
	}
}

void Map::draw(sf::RenderWindow& w)
{
	for (int i = 0; i < mapSize.x; i++)
	{
		for (int j = 0; j < mapSize.y; j++)
		{
			tiles[i][j].circle.setPosition(6 * i + 3, 6 * j + 3);	//	Makes sure the squares are properly placed and aligned on the board
			tiles[i][j].draw(w);
		}
	}
}

void Map::generate()
{
	int random;
	roombaPos = sf::Vector2i(1, 1);
	direction = sf::Vector2i(0, 1);
	temp = sf::Vector2i((mapSize.x/2)-1,(mapSize.y/2)-1);
	for (int i = 1; i < mapSize.x-1; i++)
	{
		for (int j = 1; j < mapSize.y-1; j++)
		{
			unobservedTiles.push_back(sf::Vector2i(i,j));
			tiles[i][j].open = false;
			tiles[i][j].state = unknown;
			tiles[i][j].seenType = fog;
			tiles[i][j].thoughtCost = cleanW;
			random = rand() % 100;


			int a = abs(mapSize.x/2 - i);		//	a and b become exact x and y distances from goal
			int b = abs(mapSize.y/2 - j);

			tiles[i][j].tilePos = sf::Vector2i(i, j);			//	Tells the tiles where in the array it is


			if (random >= walls+dirtAmount)
			{
				tiles[i][j].type = clean;
				tiles[i][j].cost = cleanW;
			}
			else if (random >= walls)
			{
				tiles[i][j].type = dirt;
				tiles[i][j].cost = dirtW;
				dirtList.push_back(sf::Vector2i(i, j));
				dirtCount++;
				tiles[i][j].dirtRate = rand() % 5;
				tiles[i][j].seen = 0;
			}
			else
			{
				tiles[i][j].type = hurdle;
				tiles[i][j].cost = hurdleW;		//	Again, somewhat redundant, never used, but eh
			}

		}
	}

	tiles[1][1].type = clean;
	tiles[1][1].cost = cleanW;
	tiles[1][1].closed = true;
	tiles[1][1].state = roomba;
	for (int i = 0; i < mapSize.x; i++)
	{
		for (size_t j = 0; j < mapSize.y; j++)
		{
			tiles[0][j].type = hurdle;
			tiles[mapSize.x - 1][j].type = hurdle;
			tiles[i][mapSize.y - 1].type = hurdle;
			tiles[i][0].type = hurdle;

			tiles[0][j].cost = hurdleW;
			tiles[mapSize.x - 1][j].cost = hurdleW;
			tiles[i][mapSize.y - 1].cost = hurdleW;
			tiles[i][0].cost = hurdleW;

			tiles[0][j].state = unknown;
			tiles[mapSize.x - 1][j].state = unknown;
			tiles[i][mapSize.y - 1].state = unknown;
			tiles[i][0].state = unknown;

			tiles[0][j].seenType = fog;
			tiles[mapSize.x - 1][j].seenType = fog;
			tiles[i][mapSize.y - 1].seenType = fog;
			tiles[i][0].seenType = fog;
			
			tiles[0][j].thoughtCost = cleanW;
			tiles[mapSize.x - 1][j].thoughtCost = cleanW;
			tiles[i][mapSize.y - 1].thoughtCost = cleanW;
			tiles[i][0].thoughtCost = cleanW;

		}
	}

}

void Map::update()
{
	openList.clear();
	closedList.clear();
	for (size_t i = 0; i < pathList.size(); i++)
	{
		tiles[pathList.at(i).x][pathList.at(i).y].state = unknown;
	}
	pathList.clear();


	scan();
	if (targetPos == sf::Vector2i(0, 0))
	{
		mindlessWander();
	}
	else
	{
		moveWithPurpose();
	}
	vacuumDirt();
	dirtReappearance();
	setType(roombaPos);
}

void Map::mindlessWander()
{
	if (!unobservedTiles.empty())
	{
		findTarget(unobservedTiles);
		moveWithPurpose();
	}
	else
	{
		if (!observedDirt.empty())
		{
			findTarget(observedDirt);
			moveWithPurpose();
		}
		else
		{
			int random;

			oldPos = roombaPos;
			roombaPos += direction;
			if (tiles[roombaPos.x][roombaPos.y].type == hurdle)
			{
				roombaPos = oldPos;
				srand(time(NULL));
				random = rand() % 2;
				if (direction.x == 0)
				{
					switch (random)
					{
					case 0:
						direction.x = -1;
						break;
					case 1:
						direction.x = 1;
						break;
					}
					direction.y = 0;
				}
				else
				{
					switch (random)
					{
					case 0:
						direction.y = -1;
						break;
					case 1:
						direction.y = 1;
						break;
					}
					direction.x = 0;
				}
			}
			else
			{
				actuallyMove();
			}
		}
	}
}

void Map::moveWithPurpose()
{
	oldPos = roombaPos;
	tiles[roombaPos.x][roombaPos.y].G = 0;
	findHCostFar();
	closedList.push_back(roombaPos);

	do
	{

		for (int i = 0; i < openList.size(); i++)
		{
			if (openList.at(i) == roombaPos)
			{
				openList.erase(openList.begin() + i);
			}
		}

		if (roombaPos.x + 1 < mapSize.x)
		{
			noPtrs(sf::Vector2i(roombaPos.x + 1, roombaPos.y));
		}
		if (roombaPos.x - 1 > 0)
		{
			noPtrs(sf::Vector2i(roombaPos.x - 1, roombaPos.y));
		}
		if (roombaPos.y + 1 < mapSize.y)
		{
			noPtrs(sf::Vector2i(roombaPos.x, roombaPos.y + 1));
		}
		if (roombaPos.y - 1 > 0)
		{
			noPtrs(sf::Vector2i(roombaPos.x, roombaPos.y - 1));
		}
		if (roombaPos != targetPos)
		{
			smallestOpen();
		}
	} while (roombaPos != targetPos);

	while (tiles[roombaPos.x][roombaPos.y].parent != oldPos)
	{
		pathList.push_back(roombaPos);
		roombaPos = tiles[roombaPos.x][roombaPos.y].parent;
	}

	actuallyMove();

}

void Map::findTarget(std::vector<sf::Vector2i> findTarget)
{
	srand(time(NULL));
	int random = rand() % findTarget.size();
	targetPos = findTarget.at(random);
	tiles[targetPos.x][targetPos.y].state = target;
}

void Map::vacuumDirt()
{
	if (tiles[roombaPos.x][roombaPos.y].type == dirt)
	{
		bool alreadyCleaned = false;
		tiles[roombaPos.x][roombaPos.y].type = clean;
		tiles[roombaPos.x][roombaPos.y].cost = cleanW;
		tiles[roombaPos.x][roombaPos.y].seen++;
		dirtCount--;
		std::cout << dirtCount << "  " << (dirtCount / dirtList.size()) * 100 << '\n';
		if (!observedDirt.empty())
		{
			for (size_t i = 0; i < observedDirt.size(); i++)
			{
				if (observedDirt.at(i) == roombaPos)
				{
					observedDirt.erase(observedDirt.begin() + i);
				}
			}
		}
		if (!cleanedList.empty())
		{
			for (size_t i = 0; i < cleanedList.size(); i++)
			{
				if (cleanedList.at(i) == roombaPos)
				{
					return;
				}
			}
			cleanedList.push_back(roombaPos);
		}
		else
		{
			cleanedList.push_back(roombaPos);
		}
	}
	else
	{
		//SKRIV HER, BENSKI JESUS CHRIST
	}
}

void Map::scan()
{
	int tempWeight = 20000;
	targetPos = sf::Vector2i(0, 0);
	for (int i = -(range); i <= range; i++)
	{
		if (roombaPos.x + i > 0 && roombaPos.x + i < mapSize.x)
		{
			for (int j = -(range); j <= range; j++)
			{
				if (roombaPos.y + j > 0 && roombaPos.y + j < mapSize.y)
				{
					if (!unobservedTiles.empty())
					{
						for (size_t x = 0; x < unobservedTiles.size(); x++)
						{
							if (unobservedTiles.at(x) == sf::Vector2i(roombaPos.x + i,roombaPos.y + j))
							{
								unobservedTiles.erase(unobservedTiles.begin() + x);
							}
						}
					}
					bool alreadySeen = false;
					if (tiles[roombaPos.x + i][roombaPos.y + j].type == dirt)
					{
						if (!observedDirt.empty())
						{
							for (size_t x = 0; x < observedDirt.size(); x++)
							{
								if (sf::Vector2i(roombaPos.x + i, roombaPos.y + j) == observedDirt.at(x))
								{
									alreadySeen = true;
								}
							}
						}
						if (!alreadySeen)
						{
							observedDirt.push_back(sf::Vector2i(roombaPos.x + i, roombaPos.y + j));
						}
					}
					

					if (tiles[roombaPos.x + i][roombaPos.y + j].type == dirt && abs(i) + abs(j) <= tempWeight)
					{
						tempWeight = abs(i) + abs(j);
						targetPos = roombaPos + sf::Vector2i(i,j);
					}
				}
			}
		}
	}
	tiles[targetPos.x][targetPos.y].state = target;
		tiles[0][0].state = unknown;
}

void Map::findHCostFar()
{
	for (int i = 0; i < mapSize.x; i++)
	{
		for (int j = 0; j < mapSize.y; j++)
		{
			tiles[i][j].H = (abs(targetPos.x - i) + abs(targetPos.y - j));
		}
	}
}

void Map::actuallyMove()
{
	setState(oldPos, unknown);
	setState(roombaPos, search);
	for (size_t i = 0; i < pathList.size(); i++)
	{
		tiles[pathList.at(i).x][pathList.at(i).y].state = path;
	}
	tiles[roombaPos.x][roombaPos.y].state = roomba;
	tiles[targetPos.x][targetPos.y].state = target;
}

void Map::noPtrs(sf::Vector2i neighbor)
{
	bool open = false;
	for (int i = 0; i < closedList.size(); i++)
	{
		if (closedList.at(i) == neighbor)
		{
			return;
		}
	}
	if (tiles[neighbor.x][neighbor.y].seenType != hurdle)
	{
		for (int i = 0; i < openList.size(); i++)
		{
			if (openList.at(i) == neighbor)
			{
				open = true;
			}
		}
		if ((tiles[neighbor.x][neighbor.y].G > (tiles[roombaPos.x][roombaPos.y].G + tiles[neighbor.x][neighbor.y].thoughtCost)) || !open)
		{
			tiles[neighbor.x][neighbor.y].G = tiles[roombaPos.x][roombaPos.y].G + tiles[neighbor.x][neighbor.y].thoughtCost;
			tiles[neighbor.x][neighbor.y].update();
			tiles[neighbor.x][neighbor.y].parent = roombaPos;
			if (!open)
			{
				openList.push_back(neighbor);
			}
		}
	}
}

void Map::setState(sf::Vector2i toChange, State newState)
{
	for (int i = -(range); i <= range; i++)
	{
		if (toChange.x + i > 0 && toChange.x + i < mapSize.x)
		{
			for (int j = -(range); j <= range; j++)
			{
				if (toChange.y + j > 0 && toChange.y + j < mapSize.y)
				{
					tiles[toChange.x + i][toChange.y + j].state = newState;
				}
			}
		}
	}
}

void Map::setType(sf::Vector2i toChange)
{
	for (int i = -(range); i <= range; i++)
	{
		if (toChange.x + i > 0 && toChange.x + i < mapSize.x)
		{
			for (int j = -(range); j <= range; j++)
			{
				if (toChange.y + j > 0 && toChange.y + j < mapSize.y)
				{
					tiles[toChange.x + i][toChange.y + j].seenType = tiles[toChange.x + i][toChange.y + j].type;
					tiles[toChange.x + i][toChange.y + j].thoughtCost = tiles[toChange.x + i][toChange.y + j].cost;
				}
			}
		}
	}
}

void Map::dirtReappearance()
{
	if (!cleanedList.empty())
	{
		frameCount++;
		frameCount = frameCount % frequency;

		if (frameCount == 0)
		{
			int random = rand() % cleanedList.size();
			int random2 = rand() % 5;
			if (tiles[cleanedList.at(random).x][cleanedList.at(random).y].dirtRate >= random2)
			{
				tiles[cleanedList.at(random).x][cleanedList.at(random).y].type = dirt;
				cleanedList.erase(cleanedList.begin() + random);
				dirtCount++;
			}
		}
	}
}

void Map::isExplored()
{
	explored = !unobservedTiles.empty();
}