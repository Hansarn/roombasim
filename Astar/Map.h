#include "Tile.h"
#include <time.h>
#include <stdlib.h>
#pragma once

class Map : public Tile
{
public:
	sf::Vector2i mapSize;
	sf::Vector2i roombaPos;	//	Used for position of current Tile
	sf::Vector2i targetPos;		//	Target position
	sf::Vector2i direction;
	sf::Vector2i oldPos;

	std::vector<sf::Vector2i> openList;	//	The open-list
	std::vector<sf::Vector2i> closedList;	//	The closed-list
	std::vector<sf::Vector2i> pathList;	//	The parent-list
	std::vector<sf::Vector2i> dirtList;
	std::vector<sf::Vector2i> cleanedList;
	std::vector<sf::Vector2i> observedDirt;
	std::vector<sf::Vector2i> unobservedTiles;

	int range;
	float dirtCount;
	int frameCount;
	bool explored;
	int lazynumber; 
	int frequency;
	int walls;
	int dirtAmount;

	std::vector<std::vector<Tile>> tiles;

	Map();
	void smallestOpen();	//	This, in essence, became our search function
	void generate();
	void draw(sf::RenderWindow& w);
	void update();
	void mindlessWander();
	void moveWithPurpose();
	void findTarget(std::vector<sf::Vector2i> targetVector);
	void scan();
	void vacuumDirt();
	void findHCostFar();
	void actuallyMove();
	void noPtrs(sf::Vector2i neighbor);
	void setState(sf::Vector2i toChange, State newState);
	void setType(sf::Vector2i toChange);
	void dirtReappearance();
	void isExplored();
};

