#include "Tile.h"

Tile::Tile()
{
	open = false; closed = false;
	parent = sf::Vector2i(0, 0);
	G = 1000000;
	H = 1000000;
	F = 2000000;
	circle.setRadius(4);
	circle.setOrigin(4, 4);
	circle.setPointCount(4);
	circle.setRotation(45);
}

void Tile::draw(sf::RenderWindow& w)
{

	switch (state)
	{
	case unknown:
		switch (seenType)
		{
		case clean:
			circle.setFillColor(sf::Color(180, 180, 180));
			break;
		case dirt:
			circle.setFillColor(sf::Color(180, 140, 0));
			break;
		case hurdle:
			circle.setFillColor(sf::Color(80, 50, 50));
			break;
		case fog:
			circle.setFillColor(sf::Color(150, 150, 150));
			break;
		}
		break;

	case roomba:
		circle.setFillColor(sf::Color::Blue);
		break;

	case target:
		circle.setFillColor(sf::Color::Red);
		break;

	case search:
		switch (seenType)
		{
		case clean:
			circle.setFillColor(sf::Color(250, 250, 250));
			break;
		case dirt:
			circle.setFillColor(sf::Color(250, 210, 70));
			break;
		case hurdle:
			circle.setFillColor(sf::Color(150, 120, 120));
			break;
		case fog:
			circle.setFillColor(sf::Color(50, 50, 50));
			break;
		}
		break;
	case path:
		circle.setFillColor(sf::Color(60, 255, 60));
		break;
	}
	w.draw(circle);
}

void Tile::update()
{
	F = H + G;
}