#include "SFML/Graphics.hpp"
#include "iostream"
#include <stdlib.h>
#include <vector>
#include <time.h>
#pragma once
enum Type { hurdle, dirt, clean, fog };
enum State { roomba, search, unknown, target, path };
const int hurdleW = 1000000;		//	Technically not needed, I suppose
const int cleanW = 1;
const int dirtW = 3;			//	Change value to see more clearly that our code works

class Tile
{
public:

	int H;						//	Distance from goal
	int G;						//	Weight from start
	int F;						//	Sum of the two
	int cost;					//	Cost of moving on the node
	int thoughtCost;
	int dirtRate;
	int seen;
	sf::Vector2i tilePos;			//	Identifiers for its position in a later array
	sf::CircleShape circle;

	Type type;
	Type seenType;
	State state;
	sf::Vector2i parent;
	sf::Vector2i temp;

	bool open;
	bool closed;

	Tile();

	void draw(sf::RenderWindow& w);
	void update();
};

